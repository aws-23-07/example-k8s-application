Set up OpenID Connect in AWS
To deploy to your S3 Bucket from GitLab, we’re going to use a GitLab CI/CD job to receive temporary credentials from AWS without needing to store secrets. To do this, we’re going to configure OIDC for ID federation between GitLab and AWS. We’ll be following the related GitLab documentation.

Add the identity provider
The first step is going to be adding GitLab as an identity and access management (IAM) OIDC provider in AWS. AWS has instructions located here, but I will walk through it step by step.

Open the IAM console inside of AWS.

iam search

On the left navigation pane, under Access management choose Identity providers and then choose Add provider. For provider type, select OpenID Connect.

For Provider URL, enter the address of your GitLab instance, such as https://gitlab.com or https://gitlab.example.com.

For Audience, enter something that is generic and specific to your application. In my case, I'm going to enter react_s3_gl. To prevent confused deputy attacks, it's best to make this something that is not easy to guess. Take a note of this value, you will use it to set the ID_TOKEN in your .gitlab-ci.yml file.

After entering the Provider URL, click Get thumbprint to verify the server certificate of your IdP. After this, go ahead and choose Add provider to finish up.

Create the permissions policy
After you create the identity provider, you need to create a permissions policy.

From the IAM dashboard, under Access management select Policies and then Create policy. Select the JSON tab and paste the following policy replacing jw-gl-react on the resource line with your bucket name.

{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": ["s3:ListBucket"],
      "Resource": ["arn:aws:s3:::jw-gl-react"]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObject"
      ],
      "Resource": ["arn:aws:s3:::jw-gl-react/*"]
    }
  ]
}
Select the Next: Tags button, add any tags you want, and then select the Next: Review button. Enter a name for your policy and finish up by creating the policy.

Configure the role
Now it’s time to add the role. From the IAM dashboard, under Access management select Roles and then select Create role. Select Web identity.

In the Web identity section, select the identity provider you created earlier. For the Audience, select the audience you created earlier. Select the Next button to continue.

If you wanted to limit authorization to a specific group, project, branch, or tag, you could create a Custom trust policy instead of a Web identity. Since I will be deleting these resources after the tutorial, I'm going to keep it simple. For a full list of supported filterting types, see the GitLab documentation.

web identity

During the Add permissions step, select the policy you created and select Next to continue. Give your role a name and click Create role.

Open the Role you just created. In the summary section, find the Amazon Resource Name (ARN) and save it somewhere secure. You will use this in your pipeline.




Deploy to your Amazon S3 bucket using a GitLab CI/CD pipeline
Inside of your project, create two CI/CD variables. The first variable should be named ROLE_ARN. For the value, paste the ARN of the role you just created. The second variable should be named S3_BUCKET. For the value, paste the name of the S3 bucket you created earlier in this post.

I have chosen to mask my variables for an extra layer of security.